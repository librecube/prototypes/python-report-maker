from setuptools import setup, find_packages


setup(
    name='python-report-maker',
    version='0.0.1',
    description='Create your reports in a swift',
    license='MIT',
    python_requires='>=3.4',
    packages=find_packages(),
    install_requires=[
        'pypandoc',
        'markdown',
        'pygments',
        'ipython >= 6.0',
        'nbformat',
        'nbconvert',
        'jupyter_client',
        'ipykernel',
    ],
)
