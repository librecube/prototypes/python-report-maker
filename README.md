# Python Report Maker

This Python package lets you easily create your reports in html and as documents
(.odt and .docx format) in no time.

## Getting Started

Install via pip:

```bash
$ pip install git+https://gitlab.com/librecube/prototypes/python-report-maker
```

## Tutorial

### Prepare the templates

To create reports you fist need to set up the content template and a document
template (the later only if you intend to produce documents).

The content template is a text file containing markdown syntax. In addition
it may contain special tags, in the form of `@tagname@`. These tags will later
be replaced with auto-generated content.

A content template thus may look like this:

`my_template.md`:

````markdown
# Report for @month@ / @year@

The results of this month are shown below.

@monthly_results@

```python
@monthly_plot@
```

## Details

Some more text...
````

The content template is enough for html output. If you want to create documents
you will also need to provide a template for it. The document template can be a
.odt or .docx file, depending on the document type you want to create.

To start off, create an empty .odt template as such (a described [here](https://pandoc.org/MANUAL.html)):

```bash
pandoc -o my_template.odt --print-default-data-file reference.odt
```

Similar for .docx:

```bash
pandoc -o my_template.docx --print-default-data-file reference.docx
```

Then open the template, modify the styles as you wish, and save the file. You
may also add logos and text in the header and footer section. Note however that
any content in the body will be ignored and wil not show up in the final output.

### Create your custom SourceGenerator class

This step is needed in order to replace your custom tags with auto-generated
content.

For example, to process the `@monthly_results@` and `@monthly_plot` tags:

```python
from report_maker import SourceGenerator

class MySourceGenerator(SourceGenerator):

    def monthly_results(self, value):
        if value > 10:
            text = "It shows an optimistic trend."
        else:
            text = "Optimization is further needed.\nPlease allocate effort."
        return text

    def monthly_plot(self, value):
        text = """
import numpy as np
from pylab import plt
x = np.linspace(0, 2*np.pi)
_ = plt.plot(x, {} * np.sin(x))
"""
        return text
```

You will need to create methods for the tags you want to replace. However, for
simple text replacements (eg. to replace `@year@` with `2020`) you don't need
to create a method for that.

> Note that the replacements can contain Python code that is to be executed
and whose result shall be shown in the document. This code is placed into a
docstring and must thus not contain trailing spaces, as shown in the above
example.

### Initialize and generate reports

First, instantiate the `ReportMaker` class as such:

```python
rm = ReportMaker(
    content_template="templates/my_template.md",
    document_template="templates/my_template.odt",
    source_generator=MySourceGenerator)
```

Then generate the source file and provide keyword arguments for the tags, then
save it into a file:

```python
rm.generate_source(month=2, year=2021, monthly_results=15, monthly_plot=5.5)
rm.save_source("my_sourcefile.pmd")
```

> Note that the source file has a .pmd extension. This is so because it adheres
to the [Pweave](http://mpastell.com/pweave/) syntax that allows to capture the
results from Python code in the markdown file.

Finally, publish this source file as html or document.

```python
rm.create_html(inputfile="my_sourcefile.pmd", outputfile="output.html")
rm.create_document(inputfile="my_sourcefile.pmd", outputfile="output.odt")
```

To not show the Python code in the document but only the generated output,
add `echo=False` to the codeblock in the template markdown file, such as:

````markdown
```python, echo=False
@monthly_plot@
```
````

## Further Notes

To add support for page breaks (only applicable in documents, not html)
you need to install following package (supports .docx only):

```bash
pip install -e git+https://github.com/pandocker/pandoc-docx-pagebreak-py
```

Then the `\newpage` in your markdown template file will be processed correctly.
