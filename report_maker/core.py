import os
import pathlib
import tempfile

import pypandoc
from pweave import Pweb


class ReportMaker:

    def __init__(
            self,
            content_template,
            document_template,
            source_generator):
        self.content_template = content_template
        self.document_template = document_template
        self.source_generator = source_generator(self.content_template)

    def generate_source(self, **kwargs):
        self.source = self.source_generator.generate(**kwargs)

    def save_source(self, filename):
        with open(filename, 'w') as f:
            f.write(self.source)

    def load_source(self, filename):
        with open(filename, 'r') as f:
            self.source = f.read()

    def create_html(self, inputfile, outputfile):
        doc = Pweb(
            inputfile, doctype="pandoc2html", informat="markdown",
            output=outputfile)
        _ = doc.weave()

    def create_document(self, inputfile, outputfile):
        doc_type = pathlib.Path(self.document_template).suffix.replace('.', '')
        with tempfile.TemporaryDirectory() as tmpdirname:
            tmpfile = os.path.join(tmpdirname, "tempfile.md")
            doc = Pweb(
                inputfile, doctype="markdown", informat="markdown",
                figdir=tmpdirname, output=tmpfile)
            _ = doc.weave()
            _ = pypandoc.convert_file(
                tmpfile, doc_type, outputfile=outputfile,
                extra_args=[
                    "--reference-doc={}".format(self.document_template),
                    "--filter=pandoc-docx-pagebreakpy"])


class SourceGenerator:

    def __init__(self, template):
        self.template = template

    def generate(self, **kwargs):
        with open(self.template, 'r') as f:
            text = f.read()
        for key, value in kwargs.items():
            try:
                cls_method = getattr(self, key)
                x = cls_method(value)
                if x is None:
                    raise ValueError(
                        "Class method {} does not provide a return value."
                        .format(cls_method))
                text = text.replace("@{}@".format(key), x)
            except AttributeError:
                if not isinstance(value, str):
                    value = str(value)
                text = text.replace("@{}@".format(key), value)
        return text
